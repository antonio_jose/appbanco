package com.example.appbanco

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var iv_saldo : ImageView
    lateinit var iv_poupanca : ImageView
    lateinit var iv_transferencia : ImageView
    lateinit var iv_fatura : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iv_saldo = findViewById(R.id.iv_saldo)
        iv_poupanca = findViewById(R.id.iv_poupanca)
        iv_transferencia = findViewById(R.id.iv_transferencia)
        iv_fatura = findViewById(R.id.iv_fatura)

        iv_saldo.setOnClickListener(this)
        iv_poupanca.setOnClickListener(this)
        iv_transferencia.setOnClickListener(this)
        iv_fatura.setOnClickListener(this)

        supportActionBar!!.title = "Home"


    }

    override fun onClick(v: View?) {

            when(v?.id){

                    R.id.iv_saldo ->{
                        startActivity(Intent(this, SaldoActivity::class.java))
                    }

                    R.id.iv_fatura ->{
                        startActivity(Intent(this, FaturaActivity::class.java))
                    }

                    R.id.iv_poupanca ->{
                        startActivity(Intent(this, PoupancaActivity::class.java))
                    }

                    R.id.iv_transferencia ->{
                        startActivity(Intent(this, TransferenciaActivity::class.java))
                    }

            }

    }
}