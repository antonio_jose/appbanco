package com.example.appbanco

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar

class SaldoActivity : AppCompatActivity() {

    lateinit var toolbar : Toolbar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saldo)

        supportActionBar!!.hide()

        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Saldo"
        toolbar.setTitleTextColor(Color.WHITE)
        toolbar.setNavigationIcon(R.drawable.ic_navigation)
        toolbar.setNavigationOnClickListener{
                startActivity(Intent(this, MainActivity::class.java))
                finish()
        }


    }
}